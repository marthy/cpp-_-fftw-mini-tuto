#include <string> //path name
#include <vector> //data 
#include <complex> //data as well
#include <iostream> //print to the consol
#include <fstream> //opening file
#include <cassert> //assertation

#include "fftw3.h"

/*The file where you want to save the exported wisdom*/
const std::string pathToFFTwisdom = "fftw_wisdom";
/*2D specifications, change here to fit your need*/
/*Declared at global variable for both exportWisdom and doFFTW function*/
size_t row = 200;
size_t col = 200;

/*How to manage FFTW plan creation*/
void exportWisdom(){

/*Wisdom exportation
What is Wisdom:
The strength of FFTW is that it uses hundreds of dft algorithms.
Each algorithms will be optimized for specific kinds of data.
This function is used to search the most optimized algorithms, save it locally,
and re-use it each time it is needed.*/


/*Creation of array buffers to generate plan
Here I'll do it with the fftw_complex type. But
std::complex can be used, using reintrepeter cast!*/
/*You can use "new" key word to alloc the arrays, but it can be slower*/
/*I have never seen any difference, but I'll stick to best patrices*/
	fftw_complex *mat_in = fftw_alloc_complex(row * col);
	fftw_complex *mat_out = fftw_alloc_complex(row * col);
	std::cout << "Matrices have been initialized!" << std::endl;

/*If you have already exported wisdom, you must import the data!
If you don't, every algorithms saved so far will be lost */

	//fftw_import_wisdom_from_filename(pathToFFTwisdom.c_str()); //Uncomment if you have already exported wisdom

/*Now, we create the plans. You can picture the plan as a structure where a stocked the
algorithm information. It takes in argument the size of the matrice, the raw pointers to
the data (in and out), the direction (FFTW_FORDWARD or FFTW_BACKWARD) and a planner flag.
Here we do both fordward and backward transforamtion with the "out-of-place" method. 
(=input and output are not the same array.
It is possible to do "in-place transformation", 
meaning that the input is overwritten by the output. It could be faster.
To do so, just put the same pointer as input and output.*/

/*More about planner flag: It is used to allow FFTW to search for various algorithms
FFTW_ESTIMATE: Plannnin is very quick, but unlikly to find the best algo
FFTW_MEASURE: Basic plan generation, it searchs among a big enough number of algo,
but unlikly to find the most optimized one
FFTW_PATIENT: Search among almost every algo available. Very likly to find the most-optimized one, but
the plan generation is a bit long
FFTW_EXHAUSTIVE: Seach among all the algo available. It will find the most-optimized algorithm, but takes... hour(s)*/

std::cout << "Starting the plan generation ...";
	fftw_plan plan = fftw_plan_dft_2d(row, col, mat_in, mat_out, FFTW_FORWARD, FFTW_MEASURE); //FFTW_MEASURE for the demo
	fftw_plan iplan = fftw_plan_dft_2d(row, col, mat_in, mat_out, FFTW_BACKWARD, FFTW_MEASURE);
	std::cout << "Plans have been generated !!" << std::endl;

/*If you used FFTW_PATIENT or EXHAUSTIVE, you can grab a coffee to wait ...*/

/*Now that wisdom have been accumulated = best algorthim have been found, we want to export the plan.
The main goal is that we don't need to wait anymore for plan generation. 
We search for the best algorithm once for all,
then we open it up each time needed!
Several ways are possible, I have choosed to used a char array to export the wisdom*/

	char *planToChar;
	planToChar = fftw_export_wisdom_to_string();

/*We open the file*/
	std::fstream myFile;
	myFile.open(pathToFFTwisdom.c_str(), std::ios::out);
/*And overwrite the newly accumulated wisdom (and the former one if you imported it before plan generation)*/
	myFile << planToChar;

/*Now we free the plans and matrice*/
	fftw_destroy_plan(plan);
	fftw_destroy_plan(iplan);
	fftw_free(mat_in);
	fftw_free(mat_out);

	std::cout << "Plan for " << row << "x" << col << " sized matrice is exported" << std::endl;

/*Congratulation you can now use the plan for real application!*/
};


/*How to use FFTW with std::complex*/
/*You should first read the exportWisdom() function*/
void doFFT(){
/*Most of the time, data are stored into vectors. We will see how to use FFTW for
real-data or complex data*/
	std::vector<double> *vec_in_real = new std::vector<double>(row * col);
	std::vector<std::complex<double>> *vec_in_complex = new std::vector<std::complex<double>>(row * col);
	for (int k(0);k < row * col;++k){ //Filling the vectors
		(*vec_in_real)[k] = k;
		(*vec_in_complex)[k] = {(double)k,(double)(k / (k + 1))};
	}

/*We now have our data to work with. We need to allocated more array : the ouputs
You can call various algorithm, but the easiest are complex to complex ones.
We thus need to convert our real data to ... complex/
The output will be complex as well*/

	auto vec_out_real = new std::vector<std::complex<double>>(row * col); //For our real data
	auto vec_out_complex = new std::vector<std::complex<double>>(row * col); //For our complex data
	auto buffer_vec_in_real = new std::vector<std::complex<double>>(row * col); //Buffer to complex
	for (size_t k(0);k < row * col;++k){ //Converting real to complex 
		(*buffer_vec_in_real)[k] = {(*vec_in_real)[k],0};
	}

/*We are now ready to generate plan. First of all, let's import our wisdom
(only if you used exportWisdom())*/
	fftw_import_wisdom_from_filename(pathToFFTwisdom.c_str());

/*We can now generate the plan*/
/*But plan takes fftw_complex pointers as arguments... Let's cast our data to fftw_complex!*/
/*Little thing to know : The plan creation is not safe for your data. It can erase it ! The best is to use garbagge
vectors for the plan generation*/
	auto temporary_vec_in = new std::vector<std::complex<double>>(row * col); //For plan creation
	auto temporary_vec_out = new std::vector<std::complex<double>>(row * col); //For plan creation

	fftw_plan forward_plan = fftw_plan_dft_2d(row, col,
		reinterpret_cast<fftw_complex *>(&(*temporary_vec_in)[0]),
		reinterpret_cast<fftw_complex *>(&(*temporary_vec_out)[0]),
		FFTW_FORWARD, FFTW_MEASURE);
	fftw_plan backward_plan = fftw_plan_dft_2d(row, col,
		reinterpret_cast<fftw_complex *>(&(*temporary_vec_in)[0]),
		reinterpret_cast<fftw_complex *>(&(*temporary_vec_out)[0]),
		FFTW_BACKWARD, FFTW_MEASURE);
/*It doesn't matter which vectors you call for the plan creation, BUT they must have the same size as your data*/
	

/*Now that the plan are created, we can do the actual fft transformation.
As we inited our plan with a temporary vector, we must provide pointers once again*/
	//Real data
	fftw_execute_dft(forward_plan,
		reinterpret_cast<fftw_complex *>(&(*buffer_vec_in_real)[0]),
		reinterpret_cast<fftw_complex *>(&(*vec_out_real)[0]));
/*We can reuse the plan as many time as needed*/
	//Complex data
	fftw_execute_dft(forward_plan,
		reinterpret_cast<fftw_complex *>(&(*vec_in_complex)[0]),
		reinterpret_cast<fftw_complex *>(&(*vec_out_complex)[0]));

	// Do things here with the fft data in vec_out

/*To see if it works, we gather the ifft in these two vec.
Note the use of backward_fft this time, to go from frequency to time domain*/
	auto buffer_result_real_data = new std::vector<std::complex<double>>(row * col);
	auto result_real_data = new std::vector<double>(row * col);
	auto result_complex_data = new std::vector<std::complex<double>>(row * col);
	fftw_execute_dft(backward_plan,
		reinterpret_cast<fftw_complex *>(&(*vec_out_complex)[0]),
		reinterpret_cast<fftw_complex *>(&(*result_complex_data)[0]));
										
	fftw_execute_dft(backward_plan,		
		reinterpret_cast<fftw_complex *>(&(*vec_out_real)[0]),
		reinterpret_cast<fftw_complex *>(&(*buffer_result_real_data)[0]));

/*Last step: retrieving your data. The fftw transf are not normalized, we have to do it manually*/
	int normFactor = row * col;
	for (size_t k(0);k < row * col;++k){
		(*result_real_data)[k] = (*buffer_result_real_data)[k].real() / normFactor;
		(*result_complex_data)[k] =
		{(*result_complex_data)[k].real() / normFactor,
		 (*result_complex_data)[k].imag() / normFactor};
	}

/*We can compare the input and output to check if ifft(fft(X)) = X*/

	assert(10 < row * col); //avoiding bounding error
	for (size_t k(0); k < 10; ++k){
		std::cout << "Comparison of #" << k << " term :\n"
			<< "I/O real : " << (*vec_in_real)[k] << " / " << (*result_real_data)[k] << "\n"
			<< "I/O complex : " << (*vec_in_complex)[k] << " / " << (*result_complex_data)[k] << "\n";
	}

/*Some clean up and that's all !*/
	fftw_free(forward_plan);
	fftw_free(backward_plan);
	delete vec_in_real, vec_in_real, buffer_vec_in_real;
	delete vec_out_real, vec_out_complex;
	delete buffer_result_real_data, result_complex_data, result_real_data;
	delete temporary_vec_in, temporary_vec_out;
};

int main(){

	exportWisdom();
	doFFT();

	return 0;
}
