# HOW TO DO FFT IN CPP ?

Using FFTW library (http://www.fftw.org/), the cpp file provide a simple exemple on how to do FFT in C++.

I used 2D data, stocked as std::vector, either real (double) or complex (double as well).
I used FFTW3.3, linking against "libfftw3-3.lib". 

I recommand you to first read the "exportWisdom()" function : I tried to explained how plan works.
Then, read the "doFFT()" function to see how to handle real data

I'm open to any suggestion to improve this little tuto!

## Go further
Check the really good FFTW doc : http://www.fftw.org/fftw3_doc/

To use float (single precision) or long, you will need to use fonctions with a trailing "f" (float) or "l" (long).
Be sure to import the given .lib.
